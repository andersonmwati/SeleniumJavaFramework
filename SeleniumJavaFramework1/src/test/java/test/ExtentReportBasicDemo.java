package test;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReportBasicDemo {
	
	private static WebDriver driver = null;

	public static void main(String[] args) {
		
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("extentReport.html");
		
		 // create ExtentReports and attach reporter(s)
        ExtentReports extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        
        // creates a toggle for the given test, adds all log events under it    
        ExtentTest test1 = extent.createTest("esse e google test", "esse e meu premiero teste googele so para ver como funciona");
      
        driver = new ChromeDriver();
        
        test1.log(Status.INFO, "esse e meu Test case (passo, nao passo)");
        driver.get("https://www.google.com");
        
        test1.pass("navigo no google.com");
        driver.findElement(By.name("q")).sendKeys("SAAAAAAAAA");
        test1.pass("prencho o test no testbox ");
        
    	driver.findElement(By.name("q")).sendKeys(Keys.ENTER);
        test1.pass("aperto o button da enter");
    	
    	driver.close();
    	driver.quit();
        test1.pass("Fechou os Browser");
        
        test1.info("O test passou");
        
        
        // calling flush writes everything to the log file
        extent.flush();
        
        
        
	}

}
