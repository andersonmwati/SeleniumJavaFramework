package test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.testng.annotations.AfterTest;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExistentReportwithTestng {

	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	WebDriver driver = null;

	@BeforeTest
	public void Setup() {

		htmlReporter = new ExtentHtmlReporter("extent.html");

		// create ExtentReports and attach reporter(s)
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		driver = new ChromeDriver();

	
	}
	

	

	@Test
	public void Test1 () throws Exception {
		ExtentTest test1 = extent.createTest("esse e meu Test case (passo, nao passo)");
		test1.log(Status.INFO, "https://www.google.com");
		driver.get("https://www.google.com");
		// info(detai

		test1.pass("escreveu)");
		driver.findElement(By.name("q")).sendKeys("SAAAAAAAAA");
		test1.pass("prencho o test no testbox ");
		driver.findElement(By.name("q")).sendKeys(Keys.ENTER);
		test1.pass("aperto o button da enter");


		test1.fail("details", MediaEntityBuilder.createScreenCaptureFromPath("screenshot.png").build());

		// test with snapshot
		test1.addScreenCaptureFromPath("screenshot.png");


	}

	@Test
	public void Test2 () throws Exception {
		ExtentTest test2 = extent.createTest("esse e meu Test case (passo, nao passo)");
		test2.log(Status.INFO, "https://www.google.com");
		driver.get("https://www.google.com");
		// info(detai

		test2.pass("escreveu)");
		driver.findElement(By.name("q")).sendKeys("SAAAAAAAAA");
		test2.pass("prencho o test no testbox ");
		driver.findElement(By.name("q")).sendKeys(Keys.ENTER);
		test2.pass("aperto o button da enter");


		test2.fail("details", MediaEntityBuilder.createScreenCaptureFromPath("screenshot.png").build());

		// test with snapshot
		

	}

	@Test
	public void Test3 () throws Exception {
		ExtentTest test3 = extent.createTest("esse e meu Test case (passo, nao passo)");
		test3.log(Status.INFO, "https://www.google.com");
		driver.get("https://www.google.com");
		// info(detai

		test3.pass("escreveu)");
		driver.findElement(By.name("q")).sendKeys("SAAAAAAAAA");
		test3.pass("prencho o test no testbox ");
		driver.findElement(By.name("q")).sendKeys(Keys.ENTER);
		test3.pass("aperto o button da enter");


		test3.fail("details", MediaEntityBuilder.createScreenCaptureFromPath("screenshot.png").build());

		// test with snapshot
		test3.addScreenCaptureFromPath("screenshot.png");
	}

	@AfterTest
	public void tearDownTest() {
		driver.close();
		driver.quit();
		System.out.println("teste completed 1 pesquisei no google automation step by step e passo!!!");
		extent.flush();
	}



	

		



}
