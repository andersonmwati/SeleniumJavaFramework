package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class GoogleSearchPageObjects {
//creation of class for each web page

	WebDriver driver = null;
 //object locators
	By textbox_search = By.id("list-ib");


	By button_search = By.name("q");
	
	//add action methods

	public  GoogleSearchPageObjects(WebDriver driver) {
		this.driver = driver;
	}



	public void SetTextInserchBox(String text) {
		driver.findElement(button_search).sendKeys(text);

	}

	public void clickSearchButton() {

		driver.findElement(button_search).sendKeys(Keys.ENTER);
	}
   
}
