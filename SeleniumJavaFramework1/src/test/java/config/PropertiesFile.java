package config;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import test.TestNgDemo;

public class PropertiesFile {
	static String projectPath = System.getProperty("user.dir");
	static Properties pop = new Properties();
	public static void main(String[] args) {
		getProperties();
		SetProperties();

	}

	public static void getProperties() {
		try {


			InputStream imput = new FileInputStream(projectPath+"/src/test/java/config/Config.properties");
			pop.load(imput);
			String browser = pop.getProperty("browser");
			System.out.println(browser);
			TestNgDemo.browserName=browser;
		}

		catch(Exception exp) {
			System.out.println(exp.getMessage());
			System.out.println(exp.getCause());
			exp.printStackTrace();
		}


	}



	public static void SetProperties() {

		try {
			OutputStream output = new FileOutputStream(projectPath+"/src/test/java/config/Config.properties");
             pop.setProperty("browser", "chrome");
             pop.store(output, null);
		}catch(Exception exp) {
			System.out.println(exp.getMessage());
			System.out.println(exp.getCause());
			exp.printStackTrace();
		}
	}
}
